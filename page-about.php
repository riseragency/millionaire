<?php
/**
 * Template Name: About
 *
 * Riser
 */

get_header();
?>
<section class='about content'>
    <div class='in wrap'>
        <img class='cedric-about not-mobile' src='<?php echo get_template_directory_uri();?>/img/cedric-about.png' alt='Cedric the Entertainer' />
        <aside class='about-text'>
            <h1><?php echo get_the_title(); ?></h1>
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            }
            ?>
        </aside>
        <aside class='meet-millionaire not-tablet'>
            <h3>meet millionaire's new host</h3>
            <?php
            $meet_host = get_field('content');
            echo $meet_host;
            ?>
        </aside>
    </div>
    <aside class='meet-millionaire tablet-only in'>
        <h3>meet millionaire's new host</h3>
        <?php echo $meet_host; ?>
    </aside>
    <div class='buckets in'>
        <?php
        $features = get_field('features', 'options');
        if (isset($features) && !empty($features)) {
            $cnt = 0;
            foreach ($features as $feature) {
                if ($cnt < 3) { ?>
                    <div class='node'>
                        <div class='bucket'>
                            <h1 class='tab <?php echo $colors[rand(0, count($colors)-1)]; ?>'><?php echo $feature['title']; ?></h1>
                            <article>
                                <h2 class='title'><a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?>><?php echo $feature['title']; ?></a></h2>
                                <?php echo $feature['content']; ?>
                                <a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?> class='learn-more'>learn more</a>
                            </article>
                        </div>
                    </div>
            <?php
                 $cnt++;
                }
            }
        }
        ?>
</section>
<?php
get_footer();
?>