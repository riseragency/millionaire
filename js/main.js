var App = {
    lwc_slides: [],
    lwc_count:0,
    lwc_timeout: null,
    home_slider_count: 1,
    home_slider_total: null,
    home_slider_timeout: null,

    init: function() {
        /**
         * Window Resize Event Handlers
         */
        this.resize();

        /**
         * Set event binds for site
         */
        this.binds();
    },

    resize: function() {
        if ($(window).width() < 750) {
            var width = $('.latest-video').width();
            var height = (width/4) * 3.5;
            $('#latest-video, #header-video').css({
                width: width,
                height: height
            });
        } else {
            $('#latest-video, #header-video').removeAttr('style');
        }
    },

    binds: function() {
        $('#mobile-header .menu-wrap li').bind('click', function() {
            var sub = $('#mobile-'+$(this).attr('data-child'));
            if (sub.is(':visible')) {
                $(this).removeClass('current-menu-item').siblings().removeClass('current-menu-item');
                sub.css({
                    'display': 'none'
                });
            } else {
                $(this).addClass('current-menu-item').siblings().removeClass('current-menu-item');
                sub.css({
                    'display': 'block'
                }).siblings().css('display', 'none');
                $('html,body').animate({scrollTop: $('#mobile-main-menu').offset().top},'slow');
            }
        });
    },

    lookwhatscoming: function() {
        var that = this;
        var slides = $('.slide');
        $.each(slides, function() {
           that.lwc_slides.push($(this));
        });
        if (that.lwc_slides[that.lwc_count]) {
            that.lwc_slides[that.lwc_count].css('display', 'block');
        }
        that.lwc_timeout = setInterval(function() {
            that.lwc_slides[that.lwc_count].fadeOut(400);
            if (that.lwc_count+1 >= that.lwc_slides.length) {
                that.lwc_count = 0;
            } else {
                that.lwc_count++;
            }
            that.lwc_slides[that.lwc_count].fadeIn(400);
        }, 6000);
    },

    homeslider: function() {
        var that = this;
        if (!that.home_slider_total) {
            that.home_slider_total = $('.stage-bg').length;
        }
        if (that.home_slider_total > 1) {
            that.home_slider_timeout = setInterval(function() {
                var window_width = $(window).width();
                var left;
                if (window_width > 749) {
                    left = 200;
                    if (window_width < 1058) {
                        left = 130;
                    }
                    var old_num = that.home_slider_count;
                    if (that.home_slider_count+1 > that.home_slider_total) {
                        that.home_slider_count = 1;
                    } else {
                        that.home_slider_count++;
                    }
                    $('.stage-bg[combo-num="'+old_num+'"]').fadeOut(400, function() {
                        $('.stage-bg[combo-num="'+that.home_slider_count+'"]').fadeIn(400);
                    }).siblings().css('display', 'none');
                    $('.cedric-profile[combo-num="'+old_num+'"]').animate({
                        left: $(window).width()
                    }, 400, function() {
                        $(this).css('display', 'none');
                    });
                    $('.cedric-profile[combo-num="'+that.home_slider_count+'"]').delay(400).css({'left': $(window).width(), 'display': 'block'}).animate({
                        left: left
                    }, 400);
                }
            }, 6000);
        }
    }
};

$(window).load(function() {
    App.init();
    App.resize();
    $(window).resize(function() {
       App.resize();
    });
});