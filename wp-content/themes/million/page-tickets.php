<?php
/**
 * Template Name: Tickets
 *
 * Riser
 */

get_header();
?>
<section class='tickets content'>
    <div class='in wrap'>
        <h1><?php echo get_the_title(); ?></h1>
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_content();
            }
        }
        ?>
    </div>
    <div class='tall ad'>

    </div>
</section>
<?php
get_footer();
?>