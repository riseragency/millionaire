<?php
/**
 * Template Name: CMN Hospitals
 *
 * Riser
 */

get_header();
?>
<section class="cmn content">
    <div class="in wrap not-mobile">
        <div class="spots-sidebar">
            <?php
                $topImage = get_field('cmn_hospitals_spot_top');
                if (!empty($topImage)) {
                    echo '<img src="'.$topImage['url'].'" alt="'.$topImage['alt'].'" />';
                }
                $middleImage = get_field('cmn_hospitals_spot_middle');
                if (!empty($middleImage)) {
                    echo '<img src="'.$middleImage['url'].'" alt="'.$middleImage['alt'].'" />';
                }
                $bottomImage = get_field('cmn_hospitals_spot_bottom');
                if (!empty($bottomImage)) {
                    echo '<img src="'.$bottomImage['url'].'" alt="'.$bottomImage['alt'].'" />';
                }
            ?>
        </div>
        <div class="content-container">
            <?php $hero = get_field('cmn_hero_image'); if (!empty($hero)) { echo '<img src="'.$hero['url'].'" alt="'.$hero['alt'].'" title="'.$hero['caption'].'" class="cmn-hero-image" />'; } ?>
            <?php the_field('cmn_headline'); ?>
            <?php the_field('cmn_hospitals_content'); ?>
            <?php the_field('cmn_action_buttons'); ?>
        </div>
    </div>
    <div class="in wrap mobile-only not-tablet">
        <?php $hero = get_field('cmn_hero_image'); if (!empty($hero)) { echo '<img src="'.$hero['url'].'" alt="'.$hero['alt'].'" title="'.$hero['caption'].'" class="cmn-hero-image" />'; } ?>
        <?php
            $topImage = get_field('cmn_hospitals_spot_top');
            if (!empty($topImage)) {
                echo '<img src="'.$topImage['url'].'" alt="'.$topImage['alt'].'" />';
            }
        ?>
        <?php the_field('cmn_headline'); ?>
        <?php
            $middleImage = get_field('cmn_hospitals_spot_middle');
            if (!empty($middleImage)) {
                echo '<img src="'.$middleImage['url'].'" alt="'.$middleImage['alt'].'" />';
            }
            $bottomImage = get_field('cmn_hospitals_spot_bottom');
            if (!empty($bottomImage)) {
                echo '<img src="'.$bottomImage['url'].'" alt="'.$bottomImage['alt'].'" />';
            }
        ?>
        <?php the_field('cmn_hospitals_content'); ?>
        <?php the_field('cmn_action_buttons'); ?>
    </div>
    <div class="buckets in">
        <?php
        $features = get_field('features', 'options');
        if (isset($features) && !empty($features)) {
            $cnt = 0;
            foreach ($features as $feature) {
                if ($cnt < 3) { ?>
                    <div class="node">
                        <div class="bucket">
                            <h1 class="tab <?php echo $colors[rand(0, count($colors)-1)]; ?>"><?php echo $feature['title']; ?></h1>
                            <article>
                                <h2 class="title"><a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?>><?php echo $feature['title']; ?></a></h2>
                                <?php echo $feature['content']; ?>
                                <a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?> class="learn-more">learn more</a>
                            </article>
                        </div>
                    </div>
            <?php
                 $cnt++;
                }
            }
        }
        ?>
</section>
<?php
get_footer();
?>