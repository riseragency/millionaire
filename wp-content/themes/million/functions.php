<?php

/**
 * Activate Custom Menus
 */
if (function_exists('register_nav_menus')) {
    register_nav_menus();
}

/**
 * Millionaire Button Shortcode
 *
 * Millionaire Button shortcode. Returns a std-btn class anchor with
 * the name of the shortcode displayed as the anchor text, and the
 * link of the shortcode as the href.
 *
 * @param $atts array of attributes
 * @return string
 */
function millionaire_button ( $atts ){
    $code = '<a class="btn std-btn"';
    if (isset($atts['link'])) {
        $code .= ' href="'.$atts['link'].'"';
    }
    if (isset($atts['target'])) {
        $code .= ' target="'.$atts['target'].'"';
    }
    $code .= '>';
    if (isset($atts['name'])) {
        $code .= $atts['name'];
    }
    $code .= '</a>';
    return $code;
}
add_shortcode( 'btn', 'millionaire_button' );

/**
 * Colors for coloring our buckets
 */
$colors = array(
    'yel',
    'pink',
    'lt-blue',
    'green'
);
