<?php
/**
 * Template Name: Video
 *
 * Riser
 */

get_header();
?>
<section class='video-p content'>
    <div class='in wrap'>
        <h1><?php echo get_the_title(); ?></h1>
        <section class='latest-video'>
            <!-- Start of Brightcove Player -->
            <div class='brightcove-player'><!--
                    By use of this code snippet, I agree to the Brightcove Publisher T and C
                    found at https://accounts.brightcove.com/en/terms-and-conditions/.
                    -->
                <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

                <div class="brightcove">
                    <object id="latest-video" class="BrightcoveExperience">
                        <param name="bgcolor" value="#000000" />
                        <param name="width" value="540" />
                        <param name="height" value="419" />
                        <param name="playerID" value="2591572484001" />
                        <param name="playerKey" value="AQ~~,AAAAAFSM3cY~,Fo3qn-s4rOC8Xyv7hYMiB2YowTLYLOQS" />
                        <param name="isVid" value="true" />
                        <param name="isUI" value="true" />
                        <param name="dynamicStreaming" value="true" />
                        <param name="wmode" value="transparent" />
                    </object>
                </div>
            </div>
            <div class='sidemod'>
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        the_content();
                    }
                }
                ?>
                <div class='ad'><img src='<?php echo get_template_directory_uri(); ?>/img/ad-placeholder-2.jpg' /></div>
            </div>
        </section>
    </div>
</section>
<?php
get_footer();
?>