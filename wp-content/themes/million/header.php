<!doctype html>
<html lang='en'>
    <head>
        <title>
            Who Wants To Be A Millionaire | <?php the_title(); ?>
        </title>

        <!-- TYPEKIT FONTS -->
        <script type="text/javascript" src="//use.typekit.net/qhz8xql.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

        <!-- CSS -->
        <link href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css' rel='stylesheet' />
        <link href='<?php echo get_template_directory_uri(); ?>/style.css' rel='stylesheet' />
        <link href='<?php echo get_template_directory_uri(); ?>/less/main.css' rel='stylesheet' />

        <!-- JS -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src='<?php echo get_template_directory_uri(); ?>/js/lib/modernizr.min.js'></script>
        <script src='<?php echo get_template_directory_uri(); ?>/js/lib/selectivizr-min.js'></script>
        <script src='<?php echo get_template_directory_uri(); ?>/js/lib/jquery.caroufredsel.packed.js'></script>
        <script src='<?php echo get_template_directory_uri(); ?>/js/lib/jquery.background.js'></script>
        <script src='<?php echo get_template_directory_uri(); ?>/js/main.js'></script>

        <!-- META -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body>
        <header id='main-header' class='not-mobile'>
            <a href='/' class='logo'><img src='<?php echo get_template_directory_uri(); ?>/img/logo.png' alt='Who Wants to be a Millionaire' /></a>
            <nav class='social-bar'>
                <?php
                global $rules;
                global $register;
                global $instagram;
                global $facebook;
                global $twitter;
                global $youtube;

                $rules = get_field('official_rules', 'options');
                $register = get_field('register', 'options');
                $instagram = get_field('instagram', 'options');
                $facebook = get_field('facebook', 'options');
                $twitter = get_field('twitter', 'options');
                $youtube = get_field('youtube', 'options');
                ?>
                <ul>
                    <?php
                    if (!empty($rules)) {
                        echo "<li><a target='_blank' href='".$rules."'>official rules</a></li>";
                    }
                    if (!empty($register)) {
                        echo "<li><a target='_blank' href='".$register."'>register</a></li>";
                    }
                    if (!empty($instagram)) {
                        echo "<li><a class='social' target='_blank' href='".$instagram."'><i class='icon-instagram'></i></a></li>";
                    }
                    if (!empty($facebook)) {
                        echo "<li><a class='social' target='_blank' href='".$facebook."'><i class='icon-facebook'></i></a></li>";
                    }
                    if (!empty($twitter)) {
                        echo "<li><a class='social' target='_blank' href='".$twitter."'><i class='icon-twitter'></i></a></li>";
                    }
                    if (!empty($youtube)) {
                        echo "<li><a class='social' target='_blank' href='".$youtube."'><i class='icon-youtube'></i></a></li>";
                    }
                    ?>
                </ul>
            </nav>
            <div class='ad not-mobile' id='header-ad'><img src='<?php echo get_template_directory_uri(); ?>/img/ad-placeholder-1.jpg' /></div>
            <menu class='menu-wrap'>
                <?php
                wp_nav_menu('2');
                ?>
            </menu>
        </header>
        <?php
        global $combos;
        $combos = get_field('combinations', 'options');
        global $bck;
        global $pic;
        $bck = $pic = '';
        if (!empty($combos)) {
            $rand = rand(0, count($combos)-1);
            $bck = $combos[$rand]['background'];
            $pic = $combos[$rand]['picture'];
        }
        ?>
        <header id='mobile-header' class='mobile-only' style='background-image: url(<?php echo $bck; ?>)'>
            <div class='in'>
                <a href='/' class='logo'><img src='<?php echo get_template_directory_uri(); ?>/img/logo.png' alt='Who Wants to be a Millionaire' /></a>
                <a href='/'><img class='cedric-profile' <?php if (!empty($pic)) { echo "src='".$pic."'"; } ?> /></a>
            </div>
            <menu id='mobile-main-menu' class='menu-wrap'>
                <ul>
                    <li data-child='menu' class='<?php if (strtolower(get_the_title()) != 'home') {echo 'active'; } ?>' id='mobile-menu-trigger'><a>menu</a>
                    </li>
                    <li data-child='share' id='mobile-share-trigger'><a>share</a></li>
                    <li data-child='look' id='mobile-look-trigger'><a>look what's coming</a></li>
                </ul>
            </menu>
        </header>
        <div class='mobile-only'>
            <ul id='mobile-menu' class='sub-menu' style='display:none'>
                <?php
                $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
                if ($_SERVER["SERVER_PORT"] != "80") {
                    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
                } else {
                    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                }
                $nav = wp_get_nav_menu_items('2');
                foreach ($nav as $item) { ?>
                    <li>
                        <a href='<?php echo $item->url; ?>'><?php echo $item->title; ?></a>
                    </li>
                <?php
                }
                ?>
            </ul>
            <ul id='mobile-share' class='sub-menu' style='display:none'>
                <?php
                if (!empty($instagram)) {
                    echo "<li><a class='social' target='_blank' href='".$instagram."'>instagram</a></li>";
                }
                if (!empty($facebook)) {
                    echo "<li><a class='social' target='_blank' href='".$facebook."'>facebook</a></li>";
                }
                if (!empty($twitter)) {
                    echo "<li><a class='social' target='_blank' href='".$twitter."'>twitter</a></li>";
                }
                if (!empty($youtube)) {
                    echo "<li><a class='social' target='_blank' href='".$youtube."'>youtube</a></li>";
                }
                ?>
            </ul>
            <ul id='mobile-look' class='sub-menu' style='display:none'>
                <?php
                global $whats_new;
                $whats_new = get_field('new_items', 'options');
                if (isset($whats_new) && !empty($whats_new)) {
                    $cnt = 0;
                    foreach ($whats_new as $item) { ?>
                        <li><span><time><?php echo $item['date']; ?></time> <?php echo $item['content']; ?></span></li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>