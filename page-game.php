<?php
/**
 * Template Name: Buy the Game
 *
 * Riser
 */

get_header();
?>
    <section class='game content'>
        <div class='in wrap new-game'>
            <hgroup class='game-headers'>
                <h2 class='blue-header'>new downloadable</h2>
                <h2 class='black-header'>millionaire game</h2>
                <table>
                    <tr>
                        <td>
                            <hr>
                        </td>
                        <td class='middle'>
                            <h2 class='available-now'>available now</h2>
                        </td>
                        <td>
                            <hr>
                        </td>
                    </tr>
                </table>
            </hgroup>
            <div class='left halves'>
                <!-- Start of Brightcove Player -->
                <div class='brightcove-player'><!--
                    By use of this code snippet, I agree to the Brightcove Publisher T and C
                    found at https://accounts.brightcove.com/en/terms-and-conditions/.
                    -->
                    <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

                    <div class="brightcove">
                        <object id="myExperience2647392996001" class="BrightcoveExperience">
                            <param name="bgcolor" value="#FFFFFF" />
                            <param name="width" value="400" />
                            <param name="height" value="300" />
                            <param name="playerID" value="2568014266001" />
                            <param name="playerKey" value="AQ~~,AAAAAFSM3cY~,Fo3qn-s4rOAxZ9OBxU5WPOLoyBpaljsm" />
                            <param name="isVid" value="true" />
                            <param name="isUI" value="true" />
                            <param name="dynamicStreaming" value="true" />
                            <param name="@videoPlayer" value="2647392996001" />
                        </object>
                    </div>
                </div>
                <h1>Who Wants to Be a Millionaire: special editions</h1>
                <article>
                    <?php echo do_shortcode(get_field('millionaire_game_description')); ?>
                </article>
            </div><div class='right halves not-mobile'>
            <?php
            $game_images = get_field('millionaire_game_images');
            foreach ($game_images as $image) {
                $classes = 'game-image';
                if (!$image['half']) {
                    $classes .= ' full';
                } else {
                    $classes .= ' half';
                } ?>
                <div class='<?php echo $classes; ?>' style='background-image:url(<?php echo $image['image']; ?>)'></div>
            <?php
            }
            ?>
            </div>
        </div>
        <div class='in wrap small-gap platforms'>
            <article><?php echo get_field('platform_header'); ?></article>
            <div class='platform-wrap'>
            <?php
            $platforms = get_field('platforms');
            foreach ($platforms as $pf) { ?>
                <div class='platform'>
                    <div><img src='<?php echo $pf['platform']; ?>' /></div><div><img src='<?php echo $pf['purchase_network']; ?>' /></div><div class='buy-now'><?php if ($pf['buy_now_link']) { echo "<a class='btn std-btn'  href='".$pf['buy_now_link']."'>Buy Now!</a>"; } ?></div>
                </div>
            <?php
            }
            ?>
            </div>
        </div>
        <div class='in wrap small-gap'>
            <div class='left halves'>
                <div class='img-wrap mobile-only'>
                    <img src='<?php echo get_field('movie_millionaire_logo'); ?>' />
                </div>
                <h1 style='text-align:left'>Who Wants to Be a movie Millionaire: special edition</h1>
                <article>
                    <?php
                    echo do_shortcode(get_field('movie_millionaire_description'));
                    ?>
                </article>
            </div><div class='right halves not-mobile'>
                <img src='<?php echo get_field('movie_millionaire_logo'); ?>' />
            </div>
        </div>
        <div class='in wrap game-copyright'>
            <table>
                <tr>
                    <td>
                        <img src='<?php echo get_template_directory_uri(); ?>/img/rated-e.png' />
                    </td>
                    <td class='middle'>
                        <img src='<?php echo get_template_directory_uri(); ?>/img/deep-silver.png' />
                    </td>
                    <td>
                        Copyright DISCLAIMER: Produced under license from Disney ABC Domestic
                        Television and 2waytraffic, a Sony Pictures Entertainment company “©
                        2013 All Rights Reserved” “Who Wants To Be A Millionaire?” and all
                        associated logos, images and trade marks are owned by 2waytraffic, a
                        Sony Pictures Entertainment company.
                    </td>
                </tr>
            </table>
        </div>
    </section>
<?php
get_footer();
?>