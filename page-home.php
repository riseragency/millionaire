<?php
/**
 * Template Name: Home
 *
 * Riser
 */

get_header();
?>
<?php
global $bck;
global $pic;
global $combos;
?>
<section class='home'>
    <section class='cedric'>
        <div class='bgs'>
            <?php
            $num = 0;
            $z_index = 200;
            foreach ($combos as $combo) {
                $num++; ?>
                <div combo-num='<?php echo $num; ?>' class='stage-bg' style='background-image: url(<?php echo $combo['background']; ?>); <?php if($num == 1) { echo "display:block; visibility: visible;"; } ?>; z-index:<?php echo $z_index--; ?>;'></div>
            <?php
            }
            ?>
        </div>
        <div class='in'>
            <aside class='cedric-content'>
                <?php
                $use_brightcove = get_field('use_video');
                $playerid = get_field('playerID');
                $playerkey = get_field('playerkey');
                if ($use_brightcove && isset($use_brightcove) && !empty($use_brightcove) && !empty($playerid) && !empty($playerkey)) {
                ?>
                <!-- Start of Brightcove Player -->
                <div class='brightcove-player'>
                    <div class="brightcove">
                        <object id="header-video" class="BrightcoveExperience">
                            <param name="bgcolor" value="#000000" />
                            <param name="width" value="480" />
                            <param name="height" value="380" />
                            <param name="playerID" value="<?php echo get_field('playerID'); ?>" />
                            <param name="playerKey" value="<?php echo get_field('playerkey'); ?>" />
                            <param name="isVid" value="true" />
                            <param name="isUI" value="true" />
                            <param name="dynamicStreaming" value="true" />
                            <param name="wmode" value="transparent" />
                        </object>
                    </div>
                </div>
                <?php
                } else {
                    $small_header = get_field('small_header');
                    $bold_header = get_field('bold_header');
                    $body = get_field('body');
                    $btn_text = get_field('button_text');
                    $btn_link = get_field('button_link');
                    ?>
                    <div class='promo'>
                        <?php
                        if (!empty($small_header)) {
                            echo "<p class='small-header'>".$small_header."</p>";
                        }
                        if (!empty($bold_header)) {
                            echo "<p class='bold-header'>".$bold_header."</p>";
                        }
                        if (!empty($body)) {
                            echo "<p class='body'>".$body."</p>";
                        }
                        if (!empty($btn_text)) { ?>
                            <a class='btn std-btn' target='_blank' <?php if (!empty($btn_link)) { echo "href='".$btn_link."'"; } ?>><?php echo $btn_text; ?></a>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                }
                ?>
            </aside>
            <?php
            $num = 0;
            foreach ($combos as $combo) {
                $num++; ?>
                <img combo-num='<?php echo $num; ?>' <?php if($num == 1) { echo "style='left:200px; display:block'"; } ?> class='cedric-profile not-mobile' src='<?php echo $combo['picture']; ?>' />
            <?php
            }
            ?>
        </div>
    </section>
    <section class='look-whats-coming not-mobile'>
        <div class='in'>
            <div class='span6 look'>look what's coming this year</div><div class='span6 slider'>
                <?php
                global $whats_new;
                if (isset($whats_new) && !empty($whats_new)) {
                    $cnt = 0;
                    foreach ($whats_new as $item) { ?>
                        <div class='slide' style='display:none'><time><?php echo $item['date']; ?></time> <?php echo $item['content']; ?></div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>
    <section class='latest-features'>
        <a class='btn feature-left'></a>
        <a class='btn feature-right'></a>
        <h1 class='feature'>latest features</h1>
        <div class='buckets'>
            <?php
            $features = get_field('features', 'options');
            if (isset($features) && !empty($features)) {
                foreach ($features as $feature) { ?>
                    <div class='node'>
                        <div class='bucket'>
                            <?php
                            if (!empty($feature['image'])) { ?>
                                <img src='<?php echo $feature['image']; ?>' />
                            <?php
                            }
                            ?>
                            <h1 class='tab <?php echo $colors[rand(0, count($colors)-1)]; ?>'><?php echo $feature['title']; ?></h1>
                            <article>
                                <h2 class='title'><a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?>><?php echo $feature['title']; ?></a></h2>
                                <?php echo $feature['content']; ?>
                                <a <?php if (!empty($feature['link'])) { echo "href='".$feature['link']."'"; } ?> class='learn-more'>learn more</a>
                            </article>
                        </div>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        <div class='clearfix'></div>
    </section>
    <section class='latest-video'>
        <h1 class='feature'>latest video</h1>
        <div class='in'>
            <div class='sidemod'>
                <div class='bucket'>
                    <h1 class='tab dk-blue'>great moments from millionaire</h1>
                    <article>
                        Watch the latest moments from
                        the show and catch up on your
                        favorite clips.
                    </article>
                </div>
                <!--<a class='btn video-prev'></a><a class='btn video-next'></a><button class='std-btn'>view more</button>-->
                <div class='ad desktop-only'><img src='<?php echo get_template_directory_uri(); ?>/img/ad-placeholder-2.jpg' /></div>
            </div>
            <!-- Start of Brightcove Player -->
            <!-- By use of this code snippet, I agree to the Brightcove Publisher T and C
                    found at https://accounts.brightcove.com/en/terms-and-conditions/. -->
            <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

            <div class='brightcove-player'>
                <div class="brightcove">
                    <object id="latest-video" class="BrightcoveExperience">
                        <param name="bgcolor" value="#000000" />
                        <param name="width" value="540" />
                        <param name="height" value="419" />
                        <param name="playerID" value="2591572484001" />
                        <param name="playerKey" value="AQ~~,AAAAAFSM3cY~,Fo3qn-s4rOC8Xyv7hYMiB2YowTLYLOQS" />
                        <param name="isVid" value="true" />
                        <param name="isUI" value="true" />
                        <param name="dynamicStreaming" value="true" />
                        <param name="wmode" value="transparent" />
                    </object>
                </div>
            </div>
            <div class='ad not-desktop'><img src='<?php echo get_template_directory_uri(); ?>/img/ad-placeholder-2.jpg' /></div>
        </div>
    </section>
</section>
<script>
    $(window).load(function() {
        /**
         * Look Whats Coming Carousel initialization
         */
        App.lookwhatscoming();
        App.homeslider();


        /**
         * CarouFredSel: a circular, responsive jQuery carousel.
         * Configuration created by the "Configuration Robot"
         * at caroufredsel.dev7studios.com
         */
        $(".buckets").carouFredSel({
            width: "100%",
            height: "auto",
            infinite:false,
            auto: false,
            align: 'center',
            items: {
                width: "variable"
            },
            prev: {
                button: '.feature-left'
            },
            next: {
                button: '.feature-right'
            },
            scroll: {
                duration: 1200,
                easing: 'quadratic'
            },
            swipe: true
        });
    });
</script>
<?php
get_footer();
?>