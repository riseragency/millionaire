<!--[if IE 8 ]></div><![endif]-->
        <footer id='main-footer'>
            <nav class='social-bar'>
                <ul>
                    <?php
                    global $instagram;
                    global $facebook;
                    global $twitter;
                    global $youtube;
                    if (!empty($instagram)) {
                        echo "<li><a class='social' target='_blank' href='".$instagram."'><i class='icon-instagram'></i></a></li>";
                    }
                    if (!empty($facebook)) {
                        echo "<li><a class='social' target='_blank' href='".$facebook."'><i class='icon-facebook'></i></a></li>";
                    }
                    if (!empty($twitter)) {
                        echo "<li><a class='social' target='_blank' href='".$twitter."'><i class='icon-twitter'></i></a></li>";
                    }
                    if (!empty($youtube)) {
                        echo "<li><a class='social' target='_blank' href='".$youtube."'><i class='icon-youtube'></i></a></li>";
                    }
                    ?>
                </ul>
            </nav>
            <menu class='footer-menu in'>
                <ul>
                    <?php
                    $nav = wp_get_nav_menu_items('3');
                    foreach ($nav as $item) { ?>
                        <li>
                            <a class='<?php echo $item->post_excerpt; ?>' target='_blank' href='<?php echo $item->url; ?>'><?php echo $item->title; ?></a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
                <p class='copyright'>&copy; Disney* ABC Domestic Television. All rights reserved.</p>
            </menu>
        </footer>
    <script>
        $(document).ready(function() {
            $(".latest-video").css({backgroundSize: "cover"});
        });
    </script>
    <!--[if IE 8 ]>
    <script>
        $(document).ready(function() {
            $(".stage-bg, look-whats-coming, .content").css({backgroundSize: "cover"});
        });
    </script>
    <![endif]-->
    </body>
</html>