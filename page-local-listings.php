<?php
/**
 * Template Name: Local Listings
 *
 * Riser
 */

get_header();
?>
<section class='listings content'>
    <div class='in wrap'>
        <h1><?php echo get_the_title(); ?></h1>
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_content();
            }
        }
        ?>
    </div>
    <div class='tall ad'>

    </div>
</section>
<script src='<?php echo get_template_directory_uri(); ?>/js/locallistings.js'></script>
<?php
get_footer();
?>